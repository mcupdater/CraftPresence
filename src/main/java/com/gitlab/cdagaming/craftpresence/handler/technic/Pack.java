package com.gitlab.cdagaming.craftpresence.handler.technic;

import com.google.gson.annotations.SerializedName;

public class Pack {
    @SerializedName("byIndex")
    public String[] byIndex;

    @SerializedName("selected")
    public String selected;
}
